package com.leomaou.pessoasbackend.services;

import com.leomaou.pessoasbackend.entities.Pessoa;
import com.leomaou.pessoasbackend.repositories.PessoaRepository;
import com.leomaou.pessoasbackend.utils.BackendUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PessoaService {

    private static Logger logger = LoggerFactory.getLogger(PessoaService.class);

    @Autowired
    private PessoaRepository repository;

    public Pessoa getPessoaById(Long id) throws Exception {
        Optional<Pessoa> pessoa = repository.findById(id);
        if (pessoa.isPresent()) {
            return pessoa.get();
        } else {
            String msg = "Pessoa não encontrada com id " + id;
            logger.error(msg);
            throw new Exception(msg);
        }
    }

    public List<Pessoa> findAll(String nome, String sexo, String nacionalidade, String naturalidade, String cpf) {
        if (!ObjectUtils.isEmpty(nome) || !ObjectUtils.isEmpty(sexo) || !ObjectUtils.isEmpty(nacionalidade) || !ObjectUtils.isEmpty(naturalidade) || !ObjectUtils.isEmpty(cpf)) {
            return repository.findPessoaByNomeAndSexoAndNacionalidadeAndNaturalidadeAndCpf(nome, sexo, nacionalidade, naturalidade, cpf);
        } else {
            return repository.findAll();
        }
    }

    public Pessoa delete(Long id) throws Exception {
        Pessoa pessoa = this.getPessoaById(id);
        repository.deleteById(id);
        return pessoa;
    }

    public Pessoa save(Pessoa pessoa) {
        return repository.save(pessoa);
    }

    public Pessoa save(Long id, Pessoa newPessoa) throws Exception {
        Optional<Pessoa> oldPessoa = repository.findById(id);

        if (oldPessoa.isEmpty()) {
            String msg = "Pessoa não encontrada pelo id " + id;
            logger.error(msg);
            throw new Exception(msg);
        }

        Pessoa pessoa = oldPessoa.get();

        if (!ObjectUtils.isEmpty(newPessoa.getEmail())) {
            pessoa.setEmail(newPessoa.getEmail());
        }

        if (!ObjectUtils.isEmpty(newPessoa.getCpf())) {
            pessoa.setCpf(newPessoa.getCpf());
        }

        if (!ObjectUtils.isEmpty(newPessoa.getDataNascimento())) {
            pessoa.setDataNascimento(newPessoa.getDataNascimento());
        }

        if (!ObjectUtils.isEmpty(newPessoa.getNome())) {
            pessoa.setNome(newPessoa.getNome());
        }

        if (!ObjectUtils.isEmpty(newPessoa.getNacionalidade())) {
            pessoa.setNacionalidade(newPessoa.getNacionalidade());
        }

        if (!ObjectUtils.isEmpty(newPessoa.getNaturalidade())) {
            pessoa.setNaturalidade(newPessoa.getNaturalidade());
        }

        if (!ObjectUtils.isEmpty(newPessoa.getSexo())) {
            pessoa.setSexo(newPessoa.getSexo());
        }

        return save(pessoa);
    }

    public List<String> validate(Long id, Pessoa newPessoa) {
        List<String> errorList = new ArrayList<>();

        // validação para cadastro ja existente
        if (id != null) {
            Optional<Pessoa> oldPessoa = repository.findById(id);
            if (oldPessoa.isEmpty()) {
                errorList.add("Não foi possivel encontrar a pessoa pelo id " + id);
            }

            if (!ObjectUtils.isEmpty(newPessoa.getCpf())) {
                String cpf = BackendUtils.cleanFormattedCPF(newPessoa.getCpf());
                if (BackendUtils.isValidCPF(cpf)) {
                    if (repository.existsByCpfAndIdNot(newPessoa.getCpf(), id)) {
                        errorList.add("Já existe uma outra pessoa cadastrada com este cpf: " + newPessoa.getCpf());
                    }
                } else {
                    errorList.add("CPF inválido");
                }
            }
        }

        //validação para cadastro novo
        if (id == null) {
            if (!ObjectUtils.isEmpty(newPessoa.getCpf())) {
                String cpf = BackendUtils.cleanFormattedCPF(newPessoa.getCpf());
                if (BackendUtils.isValidCPF(cpf)) {
                    if (repository.existsByCpf(BackendUtils.getFormattedCPF(newPessoa.getCpf()))) {
                        errorList.add("Já existe uma outra pessoa cadastrada com este cpf: " + newPessoa.getCpf());
                    }
                } else {
                    errorList.add("CPF inválido");
                }
            } else {
                errorList.add("É obrigatório enviar um cpf para novos cadastros");
            }
        }

        // validação para cadastro novo e ja existente
        if (!ObjectUtils.isEmpty(newPessoa.getEmail())) {
            if (!BackendUtils.isValidEmail(newPessoa.getEmail())) {
                errorList.add("Email " + newPessoa.getEmail() + " é inválido");
            }
        }

        return errorList;
    }

}
