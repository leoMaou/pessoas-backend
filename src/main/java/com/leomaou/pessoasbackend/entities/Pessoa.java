package com.leomaou.pessoasbackend.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.leomaou.pessoasbackend.utils.BackendUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.util.ObjectUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "pessoas")
@Getter @Setter @NoArgsConstructor
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAtualizado;

    private String nome;

    private String sexo;

    private String email;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dataNascimento;

    private String naturalidade;

    private String nacionalidade;

    @Column(unique=true)
    private String cpf;

    public String getCpf() {
        if(!ObjectUtils.isEmpty(cpf)) {
            cpf = BackendUtils.cleanFormattedCPF(cpf);
            cpf = BackendUtils.getFormattedCPF(cpf);
        }
        return cpf;
    }

    public void setCpf(String cpf) {
        if(!ObjectUtils.isEmpty(cpf)) {
            cpf = BackendUtils.cleanFormattedCPF(cpf);
            cpf = BackendUtils.getFormattedCPF(cpf);
        }
        this.cpf = cpf;
    }
}
