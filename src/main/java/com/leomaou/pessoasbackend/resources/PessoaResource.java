package com.leomaou.pessoasbackend.resources;

import com.leomaou.pessoasbackend.entities.Pessoa;
import com.leomaou.pessoasbackend.services.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class PessoaResource {

    @Autowired
    private PessoaService service;

    @GetMapping(value = "/v1/pessoas")
    public ResponseEntity<Map<String, List<Pessoa>>> findAll(@RequestParam(value = "nome", required = false) String nome,
                                                @RequestParam(value = "sexo", required = false) String sexo,
                                                @RequestParam(value = "nacionalidade", required = false) String nacionalidade,
                                                @RequestParam(value = "naturalidade", required = false) String naturalidade,
                                                @RequestParam(value = "cpf", required = false) String cpf) {

        Map<String,  List<Pessoa>> response = new HashMap<String,  List<Pessoa>>();
        List<Pessoa> pessoas = service.findAll(nome, sexo, nacionalidade, naturalidade, cpf);
        response.put("response",pessoas);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/v1/pessoas/{id}")
    public ResponseEntity<Map<String, Object>> findById(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            Pessoa pessoa = service.getPessoaById(id);
            response.put("response",pessoa);
            return ResponseEntity.ok(response);
        }catch (Exception e) {
            e.printStackTrace();
            response.put("messages",e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }

    @PostMapping(value = "/v1/pessoas")
    public ResponseEntity<Map<String, Object>> save(@RequestBody Pessoa newPessoa) {
        Map<String, Object> response = new HashMap<String, Object>();
        List<String> errorList;
        errorList = service.validate(null,newPessoa);

        if (!errorList.isEmpty()) {
            response.put("messages", errorList);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
        }

        response.put("response",  service.save(newPessoa));
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = "/v1/pessoas/{id}")
    public ResponseEntity<Map<String, Object>> update(@PathVariable Long id, @RequestBody Pessoa newPessoa) {
        Map<String, Object> response = new HashMap<String, Object>();
        List<String> errorList = service.validate(id,newPessoa);

        if (!errorList.isEmpty()) {
            response.put("messages", errorList);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
        }

        try {
            Pessoa pessoa = service.save(id, newPessoa);
            response.put("response", pessoa);
            return ResponseEntity.ok(response);
        }catch (Exception e) {
            e.printStackTrace();
            response.put("messages",e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @DeleteMapping("/v1/pessoas/{id}")
    public ResponseEntity<Map<String, Object>> deleteById(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<String, Object>();

        try {
            Pessoa pessoa = service.delete(id);
            response.put("response", pessoa);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("message", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
