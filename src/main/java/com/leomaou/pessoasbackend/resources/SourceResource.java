package com.leomaou.pessoasbackend.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/source")
public class SourceResource {

    @GetMapping
    public ResponseEntity<Map<String, String>> getSource() {
        Map<String, String> response = new HashMap<String, String>();
        response.put("Backend", "https://gitlab.com/leoMaou/pessoas-backend");
        response.put("Frontend", "https://gitlab.com/leoMaou/pessoas-frontend");

        return ResponseEntity.ok(response);
    }
}
