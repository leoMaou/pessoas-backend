package com.leomaou.pessoasbackend.repositories;

import com.leomaou.pessoasbackend.entities.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {


    @Query("SELECT p FROM Pessoa p WHERE " +
            "(:nome is null or p.nome like %:nome%) " +
            "and (:sexo is null or p.sexo = :sexo)" +
            "and (:nacionalidade is null or p.nacionalidade = :nacionalidade)" +
            "and (:naturalidade is null or p.naturalidade = :naturalidade)" +
            "and (:cpf is null or p.cpf = :cpf)")
    List<Pessoa> findPessoaByNomeAndSexoAndNacionalidadeAndNaturalidadeAndCpf(@Param("nome") String nome,
                                                                              @Param("sexo") String sexo,
                                                                              @Param("nacionalidade") String nacionalidade,
                                                                              @Param("naturalidade") String naturalidade,
                                                                              @Param("cpf") String cpf);

    boolean existsByCpf(String cpf);
    boolean existsByCpfAndIdNot(String cpf, Long id);
}
