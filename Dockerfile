FROM openjdk:11

RUN mkdir /data
WORKDIR /data
ADD target/pessoas-backend-0.0.1-SNAPSHOT.jar /data/pessoas-backend.jar
EXPOSE 8080
VOLUME /data

ENTRYPOINT ["java", "-jar", "/data/pessoas-backend.jar"]