# Pessoas Backend

Api crud de pessoas com basic auth

### To Run

```bash
./run.sh
```

### To build and send to dockerhub

```bash
./build.sh
./create-image.sh
./push.sh
```

### Docker up

#### Criar network
```bash
docker network create pessoas-network
```

#### Criar MYSQL
```bash
docker run -d --restart unless-stopped --name pessoas-mysql \
   --network=pessoas-network --hostname=mysql -p 3306:3306 \
   -e MYSQL_ROOT_PASSWORD=1 \
   mysql:5.6
        
docker exec -it pessoas-mysql /bin/bash
mysql -u root -p
create database pessoas_backend;
```

```sql
INSERT INTO pessoas (cpf, data_nascimento, email, nacionalidade, naturalidade, nome, sexo)
VALUES ( '100.361.080-30', '1997-07-27', 'emailTeste@hotmail.com', 'Brasil', 'criciumense', 'Emerson', 'Masculino');
```

#### API Pessoas backend

Caso queira rodar local, altere a conexão do mysql para da sua maquina.

```bash
docker run -d --restart unless-stopped --name=pessoas-backend \
 --network=pessoas-network --hostname=pessoas-backend -p 8080:8080  \
 --log-opt max-size=10m --log-opt max-file=5 \
 -e DB_URL=jdbc:mysql://pessoas-mysql:3306/pessoas_backend \
 -e DB_USER=root \
 -e DB_PASS=1 \
 leomaou/pessoas-backend
```